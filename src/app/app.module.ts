import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { FormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './features/shop/store/catalog/items/items.effects';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import { NavbarComponent } from './core/navbar.component';
import { routerReducer, RouterReducerState, RouterState, StoreRouterConnectingModule } from '@ngrx/router-store';
import { RouterEffects } from './core/store/router/router.effects';
import { authReducer, AuthState } from './core/store/auth/auth.reducer';
import { AuthEffects } from './core/store/auth/auth.effects';
import { AuthInterceptor } from './core/store/auth/auth.interceptor';
import { IfLoggedDirective } from './core/store/auth/if-logged.directive';
import { environment } from '../environments/environment';
import { metaReducers } from './core/store/debug.metareducer';

export interface AppState {
  theme: string;
  router: RouterReducerState;
  authentication: AuthState;
}

const reducers: ActionReducerMap<AppState> = {
  theme: () => 'dark',
  router: routerReducer,
  authentication: authReducer
};

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    IfLoggedDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, { metaReducers }),
    !environment.production ? StoreDevtoolsModule.instrument({
      maxAge: 15
    }) : [],
    EffectsModule.forRoot([ RouterEffects, AuthEffects ]),
    StoreRouterConnectingModule.forRoot({
      routerState: RouterState.Minimal
    })
  ],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
