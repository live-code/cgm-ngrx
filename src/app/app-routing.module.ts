import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/store/auth/auth.guard';

const routes: Routes = [
  {
    path: 'admin',
    canActivate: [AuthGuard],
    loadChildren: () => import('./features/admin/admin.module').then(m => m.AdminModule) },
  { path: 'login', loadChildren: () => import('./features/login/login.module').then(m => m.LoginModule) },
  { path: 'catalog',  canActivate: [AuthGuard], loadChildren: () => import('./features/shop/catalog.module').then(m => m.CatalogModule) },
  { path: 'pexels-video', loadChildren: () => import('./features/pexels-video/pexels-video.module').then(m => m.PexelsVideoModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
