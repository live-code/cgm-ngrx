import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Item } from '../model/item';
import { getItems, getItemsTotal } from '../features/shop/store/catalog/items/items.selectors';
import { Store } from '@ngrx/store';
import { getItemsNotification } from '../features/shop/store/catalog/notifications/notification.selectors';

@Component({
  selector: 'cgm-navbar',
  template: `

    <button routerLink="login">login</button>
    <button routerLink="pexels-video">pexels-video</button>
    <button routerLink="admin" *cgmIfLogged>admin</button>
    <button routerLink="catalog" *cgmIfLogged>catalog</button>
  `,
  styles: [
  ]
})
export class NavbarComponent implements OnInit {
  constructor(private store: Store) {}

  ngOnInit(): void {
  }

}
