// a metaReducers for debugging
import { ActionReducer, MetaReducer } from '@ngrx/store';
import { AppState } from '../../app.module';
import { environment } from '../../../environments/environment';

export const metaReducers: MetaReducer<AppState>[] = !environment.production ? [debug] : [];

export function debug(reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    if (!action.type.includes('@ngrx/router-store')) {
      console.log('STATE', state);
      console.log('ACTION', action);
    }
    return reducer(state, action);
  };
}
