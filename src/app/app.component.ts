import { Component, ViewChild } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { filter, tap } from 'rxjs/operators';

@Component({
  selector: 'cgm-root',
  template: `
    
    <h1>NGRX</h1>
    <cgm-navbar></cgm-navbar>
    
    <div style="margin-top: 30px" class="light">
      <router-outlet></router-outlet>
    </div>
    
  `,
  styles: []
})
export class AppComponent {

}
