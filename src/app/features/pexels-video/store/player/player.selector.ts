import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';

export const getPexelsVideoFeature = createFeatureSelector<PexelsVideoState>('pexels-video')

export const getCurrentVideo = createSelector(
  getPexelsVideoFeature,
  state => state.player.currentVideo
);

export const getCurrentVideoUrl = createSelector(
  getPexelsVideoFeature,
  state => state.player.currentVideo?.video_files[0].link
);

export const getCurrentVideoId = createSelector(
  getPexelsVideoFeature,
  state => state.player.currentVideo?.id
);
