import { createReducer, on } from '@ngrx/store';
import { Video } from '../../model/pexels-video-response';
import { showVideo } from './player.actions';

export interface PexelsPlayerState {
  currentVideo: Video;
}

const initialState: PexelsPlayerState = {
  currentVideo: null
};

export const playerReducer = createReducer(
  initialState,
  on(showVideo, (state, action) => ({ ...state, currentVideo: action.video}))
)
