import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';

export const getPexelsVideoFeature = createFeatureSelector<PexelsVideoState>('pexels-video')

export const getPexelsFilters = createSelector(
  getPexelsVideoFeature,
  state => state.filters
);

export const getPexelsFiltersMinRes = createSelector(
  getPexelsFilters,
  state => state.minResolution
);
