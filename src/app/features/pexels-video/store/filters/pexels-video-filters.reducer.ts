import { createReducer, on } from '@ngrx/store';
import { filterByMinResolution } from './pexels-video-filters.actions';
import { searchVideos } from '../search/pexels-video-search.actions';

export interface PexelsFiltersState {
  minResolution: number;
}

export const initialState: PexelsFiltersState = {
  minResolution: 0
};

export const filtersReducer = createReducer(
  initialState,
  // on(searchVideos, () => ({ minResolution: 0 })),
  on(filterByMinResolution, (state, action) => ({ ...state, minResolution: action.minResolution}))
);
