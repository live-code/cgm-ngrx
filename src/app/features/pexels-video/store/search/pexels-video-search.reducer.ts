import { Video } from '../../model/pexels-video-response';
import { createReducer, on } from '@ngrx/store';
import * as SearchActions from './pexels-video-search.actions';

export interface PexelsVideoSearchState {
  list: Video[];
  hasError: boolean;
  pending: boolean;
  text: string;
}

export const initialState: PexelsVideoSearchState = {
  list: [],
  hasError: false,
  pending: false,
  text: '',
};

export const searchReducer = createReducer(
  initialState,
  on(SearchActions.searchVideos, (state, action) => ({...state, text: action.text, pending: true, hasError: false})),
  on(SearchActions.searchVideosSuccess, (state, action) => ({ ...state, list: action.items, pending: false, hasError: false})),
  on(SearchActions.searchVideosFails, (state, action) => ({...state, pending: false, hasError: true})),
);


