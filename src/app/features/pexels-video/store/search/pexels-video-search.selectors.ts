import { createFeatureSelector, createSelector } from '@ngrx/store';
import { PexelsVideoState } from '../../pexels-video.module';
import { getCurrentVideo } from '../player/player.selector';
import { getPexelsFiltersMinRes } from '../filters/pexels-video-filters.selectors';

export const getPexelsVideoFeature = createFeatureSelector<PexelsVideoState>('pexels-video')

export const getPexelsVideo = createSelector(
  getPexelsVideoFeature,
  getPexelsFiltersMinRes,
  (state, minRes) => state.search.list.filter(item => item.width > minRes)
);

export const getPexelsVideo2 = createSelector(
  getPexelsVideoFeature,
  (state) => state.search.list.filter(item => item.width > state.filters.minResolution)
);


export const getPexelsVideoSearchPending = createSelector(
  getPexelsVideoFeature,
  state => state.search.pending
);
export const getPexelsVideoSearchError = createSelector(
  getPexelsVideoFeature,
  state => state.search.hasError
);

export const getPexelsVideoSearchText = createSelector(
  getPexelsVideoFeature,
  state => state.search.text
);

export const getPexelsVideoPictures = createSelector(
  getPexelsVideo,
  getCurrentVideo,
  (videos, currentVideo) => {
    return videos.find(v => v.id === currentVideo?.id)?.video_pictures
      .map(pic => pic.picture);
  }
);

// as alternative without selectors
export const getPexelsVideoPictures2 = createSelector(
  getPexelsVideoFeature,
  (state) => {
    return state.search.list.find(v => v.id === state.player.currentVideo.id).video_pictures
      .map(pic => pic.picture);
  }
);
