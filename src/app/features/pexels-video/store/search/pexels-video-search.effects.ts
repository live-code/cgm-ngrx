import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { searchVideos, searchVideosFails, searchVideosSuccess } from './pexels-video-search.actions';
import { catchError, map, switchMap, tap, withLatestFrom } from 'rxjs/operators';
import { PexelsVideoResponse } from '../../model/pexels-video-response';
import { of } from 'rxjs';
import { Store } from '@ngrx/store';
import { selectCurrentRoute, selectUrl } from '../../../../core/store/router/router.selectors';
import { go } from '../../../../core/store/router/router.actions';

@Injectable()
export class PexelsVideoSearchEffects {

  searchVideoEffects$ = createEffect(() => this.actions$.pipe(
    ofType(searchVideos),
    withLatestFrom(this.store.select(selectUrl)),
    switchMap(([action, url]) => {
      console.log(url); // not used. For demo purpose
      return this.http.get<PexelsVideoResponse>(
        'https://api.pexels.com/videos/search?per_page=10&query=' + action.text,
        { headers: { Authorization: '563492ad6f9170000100000189ac030285b04e35864a33b95c2838be' } }
      )
        .pipe(
          // map(response => go({ path: 'login'})),
          map(response => searchVideosSuccess({ items: response.videos})),
          catchError(err => of(searchVideosFails()))
        );
    })
  ));

  constructor(
    private actions$: Actions,
    private http: HttpClient,
    private store: Store
  ) {
  }
}
