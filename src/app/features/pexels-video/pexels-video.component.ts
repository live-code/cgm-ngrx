import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { searchVideos } from './store/search/pexels-video-search.actions';
import { Observable } from 'rxjs';
import { Video } from './model/pexels-video-response';
import {
  getPexelsVideo,
  getPexelsVideoPictures,
  getPexelsVideoSearchPending,
  getPexelsVideoSearchText
} from './store/search/pexels-video-search.selectors';
import { showVideo } from './store/player/player.actions';
import { filterByMinResolution } from './store/filters/pexels-video-filters.actions';
import { getCurrentVideoId, getCurrentVideoUrl } from './store/player/player.selector';
import { getPexelsFiltersMinRes } from './store/filters/pexels-video-filters.selectors';

@Component({
  selector: 'cgm-pexels-video',
  template: `
    <div *ngIf="videosPending$ | async">loading....</div>

    <input type="text" (keydown.enter)="searchVideoHandler($event)" [ngModel]="videoSearchText$ | async">
    <hr>

    <div *ngIf="getCurrentVideoUrl$ | async as video">
      <video width="300" [src]="video" controls>
        Your browser does not support the video tag.
      </video>
    </div>
    
    <!--Image video Preview-->
    <div *ngIf="videoPictures$ | async as videoPictures">
      <img
        *ngFor="let picture of videoPictures"
        [src]="picture" alt="" width="100" 
      >
    </div>
    
    <!--Video List Filters-->
    <select (change)="filterByResolution($event)" [ngModel]="getPexelsFiltersMinRes$ | async">
      <option>0</option>
      <option>2000</option>
      <option>3000</option>
      <option>4000</option>
    </select>
    
    <!--Video List-->
    <div *ngFor="let video of videos$ | async">
      <img [src]="video.image" [alt]="video.user.name" width="100"
           [ngClass]="{active: video.id === (getCurrentVideoId$ | async)}"
           (click)="showPreview(video)">
      {{video.width}} x {{video.height}}
    </div>
    
  `,
  styles: [`
    .active {
      padding: 10px;
      border: 1px solid red
    }
  `]
})
export class PexelsVideoComponent {
  videos$: Observable<Video[]> = this.store.select(getPexelsVideo);
  videosPending$: Observable<boolean> = this.store.select(getPexelsVideoSearchPending);
  videoPictures$: Observable<string[]> = this.store.select(getPexelsVideoPictures);
  getCurrentVideoUrl$: Observable<string> =  this.store.pipe(select(getCurrentVideoUrl));
  getCurrentVideoId$: Observable<number> =  this.store.pipe(select(getCurrentVideoId));
  getPexelsFiltersMinRes$: Observable<number> =  this.store.pipe(select(getPexelsFiltersMinRes));
  videoSearchText$: Observable<string> =  this.store.pipe(select(getPexelsVideoSearchText));

  constructor(private store: Store) {
    this.store.dispatch(searchVideos({ text: 'nature' }));
  }

  searchVideoHandler(event: KeyboardEvent): void {
    this.store.dispatch(
      searchVideos({ text: (event.target as HTMLInputElement).value})
    );
  }

  showPreview(video: Video): void {
    this.store.dispatch(showVideo({ video }));
  }

  filterByResolution(event: Event): void {
    const minResolution = +(event.target as HTMLSelectElement).value;
    this.store.dispatch(filterByMinResolution({ minResolution }));
  }

}
