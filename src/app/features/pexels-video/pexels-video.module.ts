import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PexelsVideoRoutingModule } from './pexels-video-routing.module';
import { PexelsVideoComponent } from './pexels-video.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { PexelsVideoSearchState, searchReducer } from './store/search/pexels-video-search.reducer';
import { EffectsModule } from '@ngrx/effects';
import { PexelsVideoSearchEffects } from './store/search/pexels-video-search.effects';
import { PexelsPlayerState, playerReducer } from './store/player/player.reducer';
import { filtersReducer, PexelsFiltersState } from './store/filters/pexels-video-filters.reducer';
import { FormsModule } from '@angular/forms';

export interface PexelsVideoState {
  search: PexelsVideoSearchState;
  player: PexelsPlayerState;
  filters: PexelsFiltersState;
}

export const reducers: ActionReducerMap<PexelsVideoState> = {
  search: searchReducer,
  player: playerReducer,
  filters: filtersReducer
};

@NgModule({
  declarations: [
    PexelsVideoComponent
  ],
  imports: [
    CommonModule,
    PexelsVideoRoutingModule,
    FormsModule,
    StoreModule.forFeature('pexels-video', reducers),
    EffectsModule.forFeature([PexelsVideoSearchEffects])
  ]
})
export class PexelsVideoModule { }
