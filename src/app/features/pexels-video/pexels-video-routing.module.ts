import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PexelsVideoComponent } from './pexels-video.component';

const routes: Routes = [{ path: '', component: PexelsVideoComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PexelsVideoRoutingModule { }
