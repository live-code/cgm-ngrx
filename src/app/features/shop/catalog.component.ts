import { Component, ViewChild } from '@angular/core';
import {  Store } from '@ngrx/store';
import { addItem, addItemSuccess, deleteItem, loadItems } from './store/catalog/items/items.actions';
import { Observable } from 'rxjs';
import { Item } from '../../model/item';
import { getItems, getItemsError } from './store/catalog/items/items.selectors';
import { NgForm } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { closeLeft, openLeft } from './store/ui/ui.actions';
import { getOpenLeft, getOpenRight } from './store/ui/ui.selectors';

@Component({
  selector: 'cgm-catalog',
  template: `
    
    <div 
      class="left-panel" 
      [ngClass]="{'open': isOpenLeft | async}"
    >
      left panel
      <button (click)="closeLeftPanelHandler()">x</button>
    </div>

    <div
      class="right-panel"
      [ngClass]="{'open': isOpenRight | async}"
    >
      right panel
      <!--<button (click)="()"></button>-->
    </div>

    <div style="background-color: red" *ngIf="error$ | async">
      errore!
    </div>
    <button (click)="openLeftHandler()">open left panel</button>
    <hr>
    <cgm-item-form (addItem)="addItemHandler($event)"></cgm-item-form>
    <cgm-list [items]="items$ | async" (deleteItem)="deleteHandler($event)"></cgm-list>
    
  `,
  styles: [`
    .left-panel {
      position: fixed;
      top: 0; bottom: 0; left: -300px; width: 300px;
      background-color: grey;
      transition: all 0.8s ease-in-out;
    }

    .right-panel {
      position: fixed;
      top: 0; bottom: 0; right: -300px; width: 300px;
      background-color: grey;
      transition: all 0.8s ease-in-out;
    }
    
    .left-panel.open {
      left: 0;
    }
    .right-panel.open {
      right: 0;
    }
  `]
})
export class CatalogComponent {
  items$: Observable<Item[]> = this.store.select(getItems);
  error$: Observable<boolean> = this.store.select(getItemsError);
  isOpenLeft: Observable<boolean> = this.store.select(getOpenLeft);
  isOpenRight: Observable<boolean> = this.store.select(getOpenRight);

  constructor(private store: Store) {
    this.store.dispatch(loadItems());
  }

  addItemHandler(item: Item): void {
    this.store.dispatch(addItem({ item }));
  }

  deleteHandler(id: number): void {
    this.store.dispatch(deleteItem({ id }));
  }

  openLeftHandler(): void {
    this.store.dispatch(openLeft());
  }

  closeLeftPanelHandler(): void {
    this.store.dispatch(closeLeft());
  }
}
