import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatalogRoutingModule } from './catalog-routing.module';
import { CatalogComponent } from './catalog.component';
import { ActionReducerMap, StoreModule } from '@ngrx/store';
import { catalogReducers, CatalogState } from './store/catalog';
import { FormsModule } from '@angular/forms';
import { uiReducer, UiState } from './store/ui/ui.reducer';
import { ListComponent } from './components/list.component';
import { ItemFormComponent } from './components/item-form.component';
import { EffectsModule } from '@ngrx/effects';
import { ItemsEffects } from './store/catalog/items/items.effects';


export interface ShopState {
  catalog: CatalogState;
  clients: any[];
  ui: UiState;
}

const reducers: ActionReducerMap<ShopState> = {
  catalog: catalogReducers,
  clients: () => [],
  ui: uiReducer
};

@NgModule({
  declarations: [
    CatalogComponent,
    ListComponent,
    ItemFormComponent
  ],
  imports: [
    CommonModule,
    CatalogRoutingModule,
    FormsModule,
    StoreModule.forFeature('shop', reducers ),
    EffectsModule.forFeature([ItemsEffects])
  ]
})
export class CatalogModule { }
