import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ShopState } from '../../catalog.module';

export const getShopFeature = createFeatureSelector<ShopState>('shop');

export const getOpenLeft = createSelector(
  getShopFeature,
  shop => shop.ui.openLeft
);

export const getOpenRight = createSelector(
  getShopFeature,
  shop => shop.ui.openRight
);
