import { createReducer, on } from '@ngrx/store';
import { closeLeft, openLeft } from './ui.actions';

export interface UiState {
  openLeft: boolean;
  openRight: boolean;
  openModal: boolean;
}

const initialState: UiState = {
  openLeft: false,
  openRight: true,
  openModal: false,
};

export const uiReducer = createReducer(
  initialState,
  on(openLeft, (state) => ({...state, openLeft: true, openRight: false })),
  on(closeLeft, (state) => ({...state, openLeft: false, openRight: true })),
);


