import { createAction } from '@ngrx/store';

export const openLeft = createAction('[ui] open left panel');
export const closeLeft = createAction('[ui] close left panel');
export const openRight = createAction('[ui] open right panel');
export const closeRight = createAction('[ui] close right panel');
