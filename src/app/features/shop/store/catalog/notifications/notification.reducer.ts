import { createReducer, on } from '@ngrx/store';
import * as ItemsActions from '../items/items.actions';


export const notificationReducer = createReducer(
  '',
  on(ItemsActions.loadItemsSuccess, () => 'Loaded Successfully'),
  on(ItemsActions.addItem, () => ''),
  on(ItemsActions.addItemSuccess, () => 'Added Successfully'),
  on(ItemsActions.deleteItem, () => ''),
  on(ItemsActions.deleteItemSuccess, () => 'Deleted Successfully'),
);

