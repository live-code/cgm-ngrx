import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ShopState } from '../../../catalog.module';

export const getShopFeature = createFeatureSelector<ShopState>('shop');

export const getItemsNotification = createSelector(
  getShopFeature,
  state => state.catalog.notify
);
