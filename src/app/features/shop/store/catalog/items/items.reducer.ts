import { createReducer, on } from '@ngrx/store';
import * as ItemsActions from './items.actions';
import { Item } from '../../../../../model/item';

export interface ItemsState {
  list: Item[];
  error: boolean;
}

const initialState: ItemsState = {
  list: [],
  error: false
};

export const itemsReducer = createReducer(
  initialState,
  on(ItemsActions.loadItemsSuccess, (state, action) => ({ list: [...state.list, ...action.items], error: false })),
  on(ItemsActions.loadItemsFailed, (state) => ({ ...state, error: true })),
  on(ItemsActions.addItem, (state) => ({ ...state, error: false })),
  on(ItemsActions.addItemSuccess, (state, action) => ({ list: [...state.list, action.item], error: false })),
  on(ItemsActions.addItemFailed, (state) => ({ list: [...state.list], error: true })),
  on(ItemsActions.deleteItem, (state) => {
    return ({ ...state, error: false })
  }),
  on(ItemsActions.deleteItemSuccess, (state, action) => ({ list: state.list.filter(item => item.id !== action.id), error: false })),
  on(ItemsActions.deleteItemFailed, (state) => ({ ...state, error: true })),

);
