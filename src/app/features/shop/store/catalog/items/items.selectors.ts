import { AppState } from '../../../../../app.module';
import { createFeatureSelector, createSelector } from '@ngrx/store';
import { ShopState } from '../../../catalog.module';


export const getShopFeature = createFeatureSelector<ShopState>('shop');

export const getItems = createSelector(
  getShopFeature,
  state => state.catalog.items.list
);

export const getItemsError = createSelector(
  getShopFeature,
  state => state.catalog.items.error
);

export const getItemsTotal = createSelector(
  getShopFeature,
  state => state.catalog.items.list.length
);
