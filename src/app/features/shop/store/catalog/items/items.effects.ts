import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType, ROOT_EFFECTS_INIT } from '@ngrx/effects';
import { HttpClient } from '@angular/common/http';
import { catchError, filter, map, switchMap, tap } from 'rxjs/operators';
import * as ItemsActions from './items.actions';
import { Item } from '../../../../../model/item';
import { fromEvent, of } from 'rxjs';

@Injectable()
export class ItemsEffects {

  loadItems$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.loadItems),
    switchMap(
      () => this.http.get<Item[]>('http://localhost:3000/users')
        .pipe(
          map(items => ItemsActions.loadItemsSuccess({ items })),
          catchError(() => of(ItemsActions.loadItemsFailed()))
        )
    )
  ));

  deleteItem$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.deleteItem),
    switchMap(
      ({ id }) => this.http.delete('http://localhost:3000/users/' + id)
        .pipe(
          map(() => ItemsActions.deleteItemSuccess({ id })),
          catchError(() => of(ItemsActions.deleteItemFailed()))
        )
    )
  ));

  addItem$ = createEffect(() => this.actions$.pipe(
    ofType(ItemsActions.addItem),
    switchMap(
      action => this.http.post<Item>('http://localhost:3000/users/', action.item)
        .pipe(
          map(item => ItemsActions.addItemSuccess({ item })),
          catchError(() => of(ItemsActions.addItemFailed()))
        )
    )
  ))
/*
  init = createEffect(() => this.actions$.pipe(
    ofType(ROOT_EFFECTS_INIT),
    tap(() => alert('ciaoooo'))
  ), { dispatch: false });
  */

  /*
  resize$ = createEffect(() => fromEvent(window, 'resize').pipe(
    tap(() => console.log('resize'))
  ), { dispatch: false });
   */

  constructor(
    private http: HttpClient,
    private actions$: Actions
  ) {


  }
}
