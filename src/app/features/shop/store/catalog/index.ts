import { ActionReducer, ActionReducerMap, combineReducers } from '@ngrx/store';
import { itemsReducer, ItemsState } from './items/items.reducer';
import { notificationReducer } from './notifications/notification.reducer';

export interface CatalogState {
  items: ItemsState;
  notify: string;
}

export const catalogReducers: ActionReducer<CatalogState> = combineReducers({
  items: itemsReducer,
  notify: notificationReducer
});
