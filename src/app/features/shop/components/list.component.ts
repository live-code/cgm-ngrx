import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Item } from '../../../model/item';

@Component({
  selector: 'cgm-list',
  template: `
    <li *ngFor="let item of items">
      {{item.name}} (  {{item.id}}  )
      <button (click)="deleteItem.emit(item.id)">del</button>
    </li>
  `,
  styles: [
  ]
})
export class ListComponent implements OnInit {
  @Input() items: Item[];
  @Output() deleteItem: EventEmitter<number> = new EventEmitter<number>();

  constructor() { }

  ngOnInit(): void {
  }

}
