import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { Item } from '../../../model/item';
import { NgForm } from '@angular/forms';
import { Actions, ofType } from '@ngrx/effects';
import { addItemSuccess } from '../store/catalog/items/items.actions';
import { Store } from '@ngrx/store';

@Component({
  selector: 'cgm-item-form',
  template: `
    <form #f="ngForm" (submit)="addItem.emit(f.value)">
      <input type="text" ngModel name="name">
      <button type="submit">ADD</button>
    </form>

  `,
  styles: [
  ]
})
export class ItemFormComponent {
  @Output() addItem: EventEmitter<Item> = new EventEmitter<Item>();
  @ViewChild('f') form: NgForm;

  constructor(private actions$: Actions) {
    this.actions$
      .pipe(ofType(addItemSuccess))
      .subscribe(() => this.form.reset());
  }

}
