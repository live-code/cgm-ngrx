import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { go } from '../../core/store/router/router.actions';
import { Credentials } from '../../core/store/auth/credentials';
import { login } from '../../core/store/auth/auth.actions';

@Component({
  selector: 'cgm-login',
  template: `
    <form #f="ngForm" (submit)="signin(f.value)" >
      <input type="text"
             placeholder="email"
             [ngModel] name="email"  required>
      <input
        type="password"
        placeholder="password"
        [ngModel]
        name="password" required>

      <button type="submit">SIGN IN</button>
    </form>

  `,
  styles: [
  ]
})
export class LoginComponent {

  constructor(private store: Store) {}

  signin(formData: Credentials): void {
    this.store.dispatch(login(formData));
  }
}
